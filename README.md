# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. ~~ Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)~~ 
3. ~~ Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones ~~ 
4. ~~ Create a short write up on how you would plan to scale this component to be able process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max) ~~

- first of all to be honest it does not make sense to handle billions of customers and millions of cuisines in memory during the run time .
- if we have to do that so it would be better to have some clusters to distruibite the data Terracotta is a good choise 
- have some more other active runing instances of the service for high availability with some data replication mecanisms 
- avoid streams as much as possible which is something i did not do becuase of other advantages like expressive style ,express quite sophisticated behaviour like our case in the topCuisines function.

Notes :
- I tried to make my solution as simple as i can without using any 3rd party library 
- I did not touch or added any  unit test as i understood from the problem description above


## Submitting your solution

+ Fork it to a private gitlab repository.
+ Put write up mentioned in point 4. into this file.
+ Send us a link to the repository, together with private ssh key that allows access (settings > repository > deploy keys).