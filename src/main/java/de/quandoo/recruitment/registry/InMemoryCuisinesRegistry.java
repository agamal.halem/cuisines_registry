package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineType;
import de.quandoo.recruitment.registry.model.Customer;
import de.quandoo.recruitment.registry.model.RegisteredCuisine;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.reverseOrder;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {


    private List<RegisteredCuisine> registeredCuisines = new ArrayList<>();


    @Override
    public void register(final Customer userId, final Cuisine cuisine) {
        if (userId == null || cuisine == null) {
            System.err.println("Registration can not be saved to database because of invalid userId or cuisine Id");
            throw new IllegalArgumentException();
        }
        RegisteredCuisine registeredCuisine = new RegisteredCuisine(userId, cuisine);
        registeredCuisines.add(registeredCuisine);

    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        return registeredCuisines.stream().filter(customerCuisine -> customerCuisine.getCuisine().equals(cuisine))
                .map(RegisteredCuisine::getCustomer).collect(Collectors.toList());
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        return registeredCuisines.stream().filter(customerCuisine -> customerCuisine.getCustomer().equals(customer))
                .map(RegisteredCuisine::getCuisine).collect(Collectors.toList());
    }

    @Override
    public List<CuisineType> topCuisines(final int n) {

        return registeredCuisines.stream()
                .map(rc -> rc.getCuisine().getCuisineType())
                .collect(groupingBy(identity(), counting()))
                .entrySet().stream()
                .sorted(Map.Entry.<CuisineType, Long>comparingByValue(reverseOrder()).thenComparing(Map.Entry.comparingByKey()))
                .limit(n)
                .map(Map.Entry::getKey)
                .collect(toList());

    }
}
