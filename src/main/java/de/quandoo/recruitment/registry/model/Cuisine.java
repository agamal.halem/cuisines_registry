package de.quandoo.recruitment.registry.model;

import java.util.Objects;

public class Cuisine {

    private CuisineType cuisineType;

    public Cuisine(CuisineType cuisineType) {
        this.cuisineType = cuisineType;
    }

    public CuisineType getCuisineType() {
        return cuisineType;
    }

    public void setCuisineType(CuisineType cuisineType) {
        this.cuisineType = cuisineType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cuisine cuisine = (Cuisine) o;
        return cuisineType == cuisine.cuisineType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(cuisineType);
    }
}
