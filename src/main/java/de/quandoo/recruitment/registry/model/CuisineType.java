package de.quandoo.recruitment.registry.model;

public enum CuisineType {
    ITALIAN, FRENCH, GERMAN;
}
