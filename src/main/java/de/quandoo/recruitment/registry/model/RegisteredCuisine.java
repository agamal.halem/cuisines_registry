package de.quandoo.recruitment.registry.model;

import java.util.Objects;

public class RegisteredCuisine {

    private Customer customer;
    private Cuisine cuisine;

    public RegisteredCuisine(Customer customer, Cuisine cuisine) {
        this.cuisine = cuisine;
        this.customer = customer;
    }

    public Cuisine getCuisine() {
        return cuisine;
    }

    public void setCuisine(Cuisine cuisine) {
        this.cuisine = cuisine;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegisteredCuisine that = (RegisteredCuisine) o;
        return Objects.equals(customer, that.customer) &&
                Objects.equals(cuisine, that.cuisine);
    }

    @Override
    public int hashCode() {

        return Objects.hash(customer, cuisine);
    }
}
