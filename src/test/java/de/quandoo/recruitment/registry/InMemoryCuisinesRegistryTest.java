package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.CuisineType;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Test;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Test
    public void shouldWork1() {
        cuisinesRegistry.register(new Customer("1"), new Cuisine(CuisineType.FRENCH));
        cuisinesRegistry.register(new Customer("2"), new Cuisine(CuisineType.GERMAN));
        cuisinesRegistry.register(new Customer("3"), new Cuisine(CuisineType.ITALIAN));

        cuisinesRegistry.cuisineCustomers(new Cuisine(CuisineType.FRENCH));
    }

    @Test
    public void shouldWork2() {
        cuisinesRegistry.cuisineCustomers(null);
    }

    @Test
    public void shouldWork3() {
        cuisinesRegistry.customerCuisines(null);
    }

    @Test
    public void thisDoesntWorkYet() {
        cuisinesRegistry.topCuisines(1);
    }


}